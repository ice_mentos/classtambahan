package org.example;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        Scanner input = new Scanner (System.in);
        System.out.print ("Masukan angka : ");
        int angka = input.nextInt();
        for (int i=0;i<angka;i++) {
            for (int j=0;j<angka;j++){
                if (i==0 || i==angka-1 || j==0 || j==angka-1)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
}
