package org.example;

import java.util.Scanner;

public class Lingkaran {
    public void showLingkaran(){
        Scanner input = new Scanner(System.in);
        double phi = 3.14;
        double r, luas,keliling;

        System.out.println("Luas & keliling Lingkaran\n");
        System.out.print("Masukkan Panjang Jari-jari : ");
        r = input.nextDouble();

        luas = phi * r * r;
        keliling = 2 * phi * r;

        System.out.println("Luas Lingkaran = " + luas);
        System.out.println("Keliling Lingakaran = " + keliling );
        System.out.println();
    }
}
